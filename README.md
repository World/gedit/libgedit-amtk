libgedit-amtk
=============

Short description
-----------------

Gedit Technology - Actions, Menus and Toolbars Kit

Longer description
------------------

libgedit-amtk is part of Gedit Technology.

It is a library based on GTK. Amtk is the acronym for “Actions, Menus and
Toolbars Kit”. It is a basic GtkUIManager replacement based on GAction. It is
suitable for both a traditional UI or a modern UI with a GtkHeaderBar.

More information
----------------

libgedit-amtk currently targets **GTK 3**.

[More information about libgedit-amtk](docs/more-information.md).
