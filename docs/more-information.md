libgedit-amtk - more information
================================

Old name
--------

libgedit-amtk was simply named Amtk until November 2022.

Amtk 5.6.x are the last versions with the name "Amtk". libgedit-amtk 5.8.x are
the first versions with the name "libgedit-amtk".

Dependencies
------------

- GLib
- GTK 3

Documentation
-------------

See the `gtk_doc` Meson option. A convenient way to read the API documentation
is with the [Devhelp](https://wiki.gnome.org/Apps/Devhelp) application.

See also other files in this directory for additional notes.

Tarballs
--------

- [libgedit-amtk tarballs](https://download.gnome.org/sources/libgedit-amtk/)
- [Old amtk tarballs](https://download.gnome.org/sources/amtk/)
